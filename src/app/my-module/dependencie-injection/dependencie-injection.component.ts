import { DependencieInjectionService } from './dependencie-injection.service';
import { Component, OnInit } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'dependencie-injection',
  templateUrl: './dependencie-injection.component.html',
  styleUrls: ['./dependencie-injection.component.css']
})
export class DependencieInjectionComponent implements OnInit {

  nameComponent: string;

  bunchOfThings: string[];

  constructor(private dpinjection: DependencieInjectionService) {
    this.nameComponent = 'Dependencie Injection';
    this.bunchOfThings = this.dpinjection.getInformations();
   }

  ngOnInit() {
  }

}
