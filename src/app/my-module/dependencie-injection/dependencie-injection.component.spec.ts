import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DependencieInjectionComponent } from './dependencie-injection.component';

describe('DependencieInjectionComponent', () => {
  let component: DependencieInjectionComponent;
  let fixture: ComponentFixture<DependencieInjectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DependencieInjectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DependencieInjectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
