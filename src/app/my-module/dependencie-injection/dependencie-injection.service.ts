import { Injectable } from '@angular/core';

@Injectable()
export class DependencieInjectionService {

  constructor() { }

  getInformations() {
    return ['nanana', 'nenene', 'ninini'];
  }
}
