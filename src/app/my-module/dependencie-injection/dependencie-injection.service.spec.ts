import { TestBed } from '@angular/core/testing';

import { DependencieInjectionService } from './dependencie-injection.service';

describe('DependencieInjectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DependencieInjectionService = TestBed.get(DependencieInjectionService);
    expect(service).toBeTruthy();
  });
});
