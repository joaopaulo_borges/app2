import { DependencieInjectionComponent } from './dependencie-injection/dependencie-injection.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DependencieInjectionService } from './dependencie-injection/dependencie-injection.service';


@NgModule({
  declarations: [
    DependencieInjectionComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DependencieInjectionComponent,
  ],
  providers: [DependencieInjectionService]
})
export class MyModule { }
