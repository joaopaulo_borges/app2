import { Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-life-cycle-hooks',
  templateUrl: './life-cycle-hooks.component.html',
  styleUrls: ['./life-cycle-hooks.component.css']
})
export class LifeCycleHooksComponent implements OnInit, OnChanges, OnDestroy {

  @Input() valorInicial = 0;
  constructor() {
    console.log('construtor');
   }

  ngOnInit() {
    console.log('ngOnInit');

  }

  ngOnDestroy() {
    console.log('ngOnDestroy');

  }

  ngOnChanges(changes: SimpleChanges): void {
    // Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    // Add '${implements OnChanges}' to the class.
    console.log('ngOnChanges: value changed on input property');
  }

}
