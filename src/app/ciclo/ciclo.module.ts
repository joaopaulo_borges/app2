import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LifeCycleHooksComponent } from './life-cycle-hooks/life-cycle-hooks.component';
import { CicloComponent } from './ciclo.component';


@NgModule({
  declarations: [
    LifeCycleHooksComponent,
    CicloComponent,
  ],
  imports: [
    CommonModule
  ],
  exports:[
    CicloComponent,
  ]
})
export class CicloModule { }
