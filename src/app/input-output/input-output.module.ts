import { OutputPropertyComponent } from './output-property/output-property.component';
import { InputPropertyComponent } from './input-property/input-property.component';
import { InputOutputComponent } from './input-output.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [InputOutputComponent,
  InputPropertyComponent,
  OutputPropertyComponent],
  imports: [
    CommonModule
  ],
  exports:[
    InputOutputComponent
  ]
})
export class InputOutputModule { }
