import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-input-output',
  templateUrl: './input-output.component.html',
  styleUrls: ['./input-output.component.css']
})
export class InputOutputComponent implements OnInit {

  foo = 'foo variable passed through input property';
  valueChangedFromOutputContador: number;
  constructor() { }

  ngOnInit() {
  }

  onValueChanging(evento: any) {
    const novoValor: number = evento.newValue;
    console.log(evento);
    console.log(novoValor);
    this.valueChangedFromOutputContador = novoValor;
  }

}
