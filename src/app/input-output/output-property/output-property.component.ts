import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'outputPropertyContador',
  templateUrl: './output-property.component.html',
  styleUrls: ['./output-property.component.css']
})
export class OutputPropertyComponent implements OnInit {

  // tslint:disable-next-line: no-input-rename
  @Input('valorInicial') valor = 0;

  @ViewChild('campoInput', {static: false}) campoValorInput: ElementRef;
  @Output() valueChanged = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  // manipulando o elemento diretament do DOM) - NOT RECOMMENDED
  // increments() {
  //   this.campoValorInput.nativeElement.value++;
  //   this.valueChanged.emit({newValue: this.campoValorInput.nativeElement.value});
  // }

  // decrements() {
  //   this.campoValorInput.nativeElement.value--;
  //   this.valueChanged.emit({newValue: this.campoValorInput.nativeElement.value});
  // }

// utilizando binding property - RECOMMENDED
  increments() {
    this.valor++;
    this.valueChanged.emit({newValue: this.valor});
  }

  decrements() {
    this.valor--;
    this.valueChanged.emit({newValue: this.valor});
  }
}
