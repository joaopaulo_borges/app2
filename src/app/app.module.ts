import { InputOutputModule } from './input-output/input-output.module';
import { MyModule } from './my-module/my-module.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BindingModule } from './binding/binding.module';
import { CicloModule } from './ciclo/ciclo.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    MyModule,
    InputOutputModule,
    BindingModule,
    CicloModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
