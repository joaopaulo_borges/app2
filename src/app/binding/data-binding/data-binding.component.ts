import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {

  name: string = 'joao pé de feijão';
  textoDoUsuario: string;

  nomeDoApp: string = "App do Chaves";

  chaves: any = {
    bruxaDo71: "Satanás é ocê?",
    quico: "Miau",
    chaves: "Outro gato"
  }

  constructor() { }
  ngOnInit() {
  }

  alterText(novoTexto: any){
    this.textoDoUsuario = novoTexto;
  }
}
