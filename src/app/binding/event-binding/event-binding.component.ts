import { Component, OnInit } from '@angular/core';
import { PopupService } from '@ng-bootstrap/ng-bootstrap/util/popup';

@Component({
  selector: 'event-binding',
  templateUrl: './event-binding.component.html',
  styleUrls: ['./event-binding.component.css']
})
export class EventBindingComponent implements OnInit {

  valorAtual: string = '';
  valorSalvo: string = '';
  isOver: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  onKeyUp(evento: KeyboardEvent) {
    this.valorAtual = (<HTMLInputElement>evento.target).value;
  }

  onEnterPressed(texto) {
    this.valorSalvo = texto
    alert(`valor Salvo: ${this.valorSalvo}`)
  }

  boo(){
    this.isOver = !this.isOver;
  }

}
