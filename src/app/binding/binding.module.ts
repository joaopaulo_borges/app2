import { FormsModule } from '@angular/forms';
import { ClassBindingComponent } from './class-binding/class-binding.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BindingComponent } from './binding.component';
import { DataBindingComponent } from './data-binding/data-binding.component';
import { EventBindingComponent } from './event-binding/event-binding.component';



@NgModule({
  declarations: [BindingComponent,
  ClassBindingComponent,
  DataBindingComponent,
  EventBindingComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    BindingComponent
  ]
})
export class BindingModule { }
